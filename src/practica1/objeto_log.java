package practica1;

import java.util.Date;

/**
 *
 * @author Pao
 */
public class objeto_log {
    private String Tipo_accion;
    private String Origen_accion;
    private String Descripcion;
    private Date Fecha;

    public objeto_log(String Tipo_accion, String Origen_accion, String Descripcion, Date Fecha) {
        this.Tipo_accion = Tipo_accion;
        this.Origen_accion = Origen_accion;
        this.Descripcion = Descripcion;
        this.Fecha = Fecha;
    }

    public objeto_log() {
    }
    

    public String getTipo_accion() {
        return Tipo_accion;
    }

    public void setTipo_accion(String Tipo_accion) {
        this.Tipo_accion = Tipo_accion;
    }

    public String getOrigen_accion() {
        return Origen_accion;
    }

    public void setOrigen_accion(String Origen_accion) {
        this.Origen_accion = Origen_accion;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

  
    
}
